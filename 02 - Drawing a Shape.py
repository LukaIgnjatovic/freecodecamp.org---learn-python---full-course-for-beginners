# Print a triangle with 4 consecutive "Print" functions.
print("   /|")
print("  / |")
print(" /  |")
print("/___|")

# Code is executed sequentially.
print("/___|")
print("   /|")
print("  / |")
print(" /  |")