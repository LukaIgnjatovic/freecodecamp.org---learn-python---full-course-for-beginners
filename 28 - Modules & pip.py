# Importing other Python files is done through the "import" function.
import Useful_Tools

# Once the file is imported, its content can be used.
print(Useful_Tools.roll_dice(10))

# "pip" is used to install external Python modules.
# pip install python-docx - This command should be executed from Command Prompt.

# "pip" is also used to uninstall external Python modules.
# pip uninstall python-docx - This command should be executed from Command Prompt.
